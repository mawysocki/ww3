import database.DBConnector;
import frames.GameFrame;
import frames.GameOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.awt.*;

public class RunnerTest {
    @Test
    public void GameFrameVisibleTest() {
        GameFrame g = new GameFrame();
        Assertions.assertFalse(g.isVisible());
    }
    @Test
    public void GameFrameSizeTest() {
        GameFrame g = new GameFrame();
        Dimension expectedSize = Toolkit.getDefaultToolkit().getScreenSize();
        Assertions.assertEquals(expectedSize, g.getSize());
    }

    @Test
    public void DBConnectorTest() {
        Assertions.assertFalse(DBConnector.isConnected());
        new GameFrame();
        Assertions.assertTrue(DBConnector.isConnected());
    }
}
