import frames.GameOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.swing.*;
import java.awt.*;

public class GameOptionsTest {
    @Test
    public void GameOptionsVisibilityTest() {
        GameOptions g = new GameOptions();
        Assertions.assertTrue(g.isVisible());
    }

    @Test
    public void GameOptionsLocationTest() {
        GameOptions g = new GameOptions();
        Point expectedLocation = new Point(100,100);

        Assertions.assertEquals(expectedLocation, g.getLocation());
    }
}
