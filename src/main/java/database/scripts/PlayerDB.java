package database.scripts;

import database.DBConnector;


import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerDB extends DBConnector {

    //Pobiera aktualny budżet gracza na bazie jego ID
    public static int getPlayerBudget(int playerID){
        String sql = "SELECT * FROM players WHERE id = " + playerID;
        ResultSet rs = executeQuery(sql);
        int budget = 0;
        try {
            if (rs.next()) {
                return rs.getInt("budget");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return budget;
    }

    //Aktualizuje budżet gracza
    public static void updatePlayerBudget(int playerID, int budget) {
        String updateSQL = String.format("UPDATE players SET budget = %s WHERE id = %s", budget, playerID);
        update(updateSQL);
    }

    //Sprawdza budżet gracza na bazie sumy przychodów wszystkich regionów do niego należacych
    public static int getDailyIncome(int playerID) {
        String sql = String.format("SELECT sum(income) FROM regions where player_id = %s", playerID);
        ResultSet rs = executeQuery(sql);
        int dailyIncome = 0;
        try {
            if (rs.next()) {
                return rs.getInt("sum(income)");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return dailyIncome;
    }
}
