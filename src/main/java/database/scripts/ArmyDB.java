package database.scripts;

import database.DBConnector;
import database.enitites.RegionArmyEntity;
import frames.Controllers;
import geo.Region;
import run.Settings;

import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArmyDB extends DBConnector {

    //Zwraca encję armi na podstawie ID
    public static RegionArmyEntity getArmyEntity(int entityID){
        String sql = "SELECT * FROM `region_army` WHERE id = " + entityID;
        ResultSet rs = executeQuery(sql);
        RegionArmyEntity entity = null;
        try {
            if (rs.next()) {
                entity = createArmyEntity(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    //Zwraca listę wszystkich encji armi dla danego regionu
    public static List<RegionArmyEntity> getRegionArmyEntities(int regionID) throws SQLException {
        String sql = "SELECT * FROM `region_army` WHERE region_id =" + regionID;
        ResultSet resultSet = executeQuery(sql);
        List<RegionArmyEntity> armyEntities = new ArrayList<>();
        while (resultSet.next()) {
            armyEntities.add(createArmyEntity(resultSet));
        }
        return armyEntities;
    }
    public static void updateArmy() {
        System.out.println("Update Army");
        //Generuje query do usuwania niezajętych już sloów
        StringBuilder sql = new StringBuilder("DELETE FROM `region_army` WHERE id NOT IN (");

        //Dwie listy zbierające informacje, które sloty muszą być dodane, będą zaktualizowane
        List<RegionArmyEntity> entitiesToAdd = new ArrayList<>();
        List<RegionArmyEntity> entitiesToUpdate = new ArrayList<>();

        //Przejście po wszystkich regionach
        for (int i = 0; i < Settings.COUNTRY_SIZE; i++) {
            for (int j = 0; j < Settings.COUNTRY_SIZE * 2; j++) {
                //Pobranie aktualnego regionu na bazie współrzędnych
                Region r = Controllers.getRegion(new Point(i,j));
                //Przejście po wszystkich slotach w regionie
                for (int u = 0; u < r.units.length; u++) {
                    //Bierze tylko niepuste sloty
                    if (r.units[u] != null) {
                        //Zbiera informacje o slocie do utworzenia encji
                        int regionNumber = r.getRegionNumber();
                        int id = (regionNumber-1) * 4 + u + 1;

                        //Dopisuje do query id dla zajętych slotów, aby je pominąć przy usuwaniu wierszy
                        sql.append(id);
                        sql.append(", ");

                        //Tworzy nową encję na podstawie frontu
                        RegionArmyEntity entity = new RegionArmyEntity(id, regionNumber, u + 1, r.units[u].type.id, r.units[u].getSize());

                        //Sprawdza czy encja o tym samym ID jest w bazie
                        RegionArmyEntity entityInDB = getArmyEntity(entity.id);
                        if (entityInDB == null) {
                            //Jeżli nie encji w bazie to znaczy, że jest nowa i należy ją dodać
                            entitiesToAdd.add(entity);
                        }
                        else {
                            //Jeżli encja jest w bazie należy sprawdzić czy różni się od poprzedniej
                            if (!entityInDB.equals(entity)) {
                                //Jeżli się różni to znaczy, że następuje aktualizacja i należy zrobić update na bazie
                                entitiesToUpdate.add(entity);
                            }
                        }
                    }
                }


            }
        }
        sql.append(")");
        //W pętli dodawany jest przecinek po każdym ID. Ostatnie ID nie powinno mieć przecinka po sobie, więc jest usuwane
        String sqlClean = sql.toString().replace(", )", ")");
//        System.out.println("SQL clean: " + sqlClean);

        //Usuwa wiersze, które nie zostały wykluczone przez zapytanie
        update(sqlClean);
        //Przechodzi po zbiorze encji, aby je dodać do bazy za pomocą INSERT
        for (RegionArmyEntity entity : entitiesToAdd) {
            insertArmyEntity(entity);
        }

        //Przechodzi po zbiorze encji, aby je zaktualizować za pomocą UPDATE
        for (RegionArmyEntity entity : entitiesToUpdate) {
            mergeArmyEntity(entity);
        }
    }

    public static void insertArmyEntity(RegionArmyEntity entity) {
        //Dodawanie nowego wpisu w bazie na podstawie utworzonej encji
        String sql = String.format("INSERT INTO `region_army`(`id`, `region_id`, `slot_id`, `unit_id`, `size`) VALUES (%s, %s, %s, %s, %s)", entity.id, entity.regionID, entity.slotID, entity.unitID, entity.size);
        update(sql);
    }

    public static void mergeArmyEntity(RegionArmyEntity entity) {
        //Aktualizowanie kolumny SIZE dla istniejącej encji
        String sql = String.format("UPDATE region_army SET size= %s WHERE id = %s", entity.size,entity.id);
        update(sql);
    }

    //Zwraca obiekt encji na podstawie rezultatu z bazy
    private static RegionArmyEntity createArmyEntity(ResultSet resultSet) {
        RegionArmyEntity entity = null;
        try {
            int id = resultSet.getInt("id");
            int regionID = resultSet.getInt("region_id");
            int slotID = resultSet.getInt("slot_id");
            int unitID = resultSet.getInt("unit_id");
            int size = resultSet.getInt("size");
            entity = new RegionArmyEntity(id, regionID, slotID, unitID, size);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }
}
