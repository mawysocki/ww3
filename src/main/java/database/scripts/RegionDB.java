package database.scripts;

import database.DBConnector;
import database.enitites.RegionEntity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegionDB extends DBConnector {

    //Zwraca encję pojedyńczego regionu na bazie jego ID
    public static RegionEntity getRegionEntity(int entityID){
        String sql = "SELECT * FROM `regions` WHERE id = " + entityID;
        ResultSet rs = executeQuery(sql);
        RegionEntity entity = null;
        try {
            if (rs.next()) {
                entity = createRegionEntity(rs);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    //Buduje encję regionu na bazie rezultatu
    private static RegionEntity createRegionEntity(ResultSet resultSet) {
        RegionEntity entity = null;
        try {
            int id = resultSet.getInt("id");
            int playerID = resultSet.getInt("player_id");
            int income = resultSet.getInt("income");
            int factory = resultSet.getInt("factory");
            int fort = resultSet.getInt("fort");
            int tower = resultSet.getInt("tower");
            entity = new RegionEntity(id, playerID, income, factory, fort, tower);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    //Aktualizuje właściciela regionu
    public static void updateCountryForRegion(int id, int contryID) {
        String updateCountrySQL = String.format("UPDATE regions SET player_id = %s WHERE id = %s", contryID, id);
        update(updateCountrySQL);
    }

}
