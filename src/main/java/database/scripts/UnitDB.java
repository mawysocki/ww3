package database.scripts;

import database.DBConnector;
import database.enitites.RegionArmyEntity;
import database.enitites.UnitEntity;
import units.Unit;
import units.Units;

import java.sql.*;
import java.util.List;

public class UnitDB extends DBConnector {

    //Pobiera encję jednostki na bazie ID
    public static UnitEntity getUnitEntity(int unitID) {
        String sql = "SELECT * FROM `units` WHERE id = " + unitID;
        ResultSet rs = executeQuery(sql);
        UnitEntity entity = null;
        try {
            if (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int hp = rs.getInt("hp");
                int size = rs.getInt("size");
                int attack = rs.getInt("attack");
                int defence = rs.getInt("defence");
                int damage = rs.getInt("damage");
                entity = new UnitEntity(id, name, hp, size, attack, defence, damage);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return entity;
    }

    //Ustawia armię w slotach dla danego regionu
    public static Unit[] setUnitsInRegion(int regionID) {
        try {
            return setUnitsFromEntity(ArmyDB.getRegionArmyEntities(regionID));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //Zwraca tablicę slotów na bazie listy encji armi
    private static Unit[] setUnitsFromEntity(List<RegionArmyEntity> entities) {
        Unit[] units = new Unit[4];
        //Przechodzi przez wszystkie przekazane encje armi
        for (int i = 0; i < entities.size(); i++) {
            //Pobiera encje jednostki na bazie ID jednostki z encji armi
            UnitEntity entity = getUnitEntity(entities.get(i).unitID);
            //Jeżeli encja jednostki jest pusta to znaczy, że nie ma armi w tym slocie, więc przypisuje null do slotu
            String unitName = entity == null ? null : entity.name;
            //Jeżli encja jednostki nie jest pusta to generuje obiekt jednostki odpowiedniego typu i przypisuje do slotu
            Unit newUnit = unitName == null ? null : new Unit(Units.valueOf(unitName), entities.get(i).size);
            units[entities.get(i).slotID - 1] = newUnit;
        }
        return units;
    }




}
