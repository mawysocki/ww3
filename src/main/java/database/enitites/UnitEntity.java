package database.enitites;

public final class UnitEntity {

    public final int id;
    public final String name;
    public final int hp;
    public final int size;
    public final int attack;
    public final int defence;
    public final int damage;

    public UnitEntity(int id, String name, int hp, int size, int attack, int defence, int damage) {
        this.id = id;
        this.name = name;
        this.hp = hp;
        this.size = size;
        this.attack = attack;
        this.defence = defence;
        this.damage = damage;
    }
}
