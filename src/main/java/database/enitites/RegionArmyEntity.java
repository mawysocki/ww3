package database.enitites;

import java.util.Objects;

public final class RegionArmyEntity {

    public final int id;
    public final int regionID;

    public final int slotID;
    public final int unitID;
    public final int size;

    public RegionArmyEntity(int id, int regionID, int slotID, int unitID, int size) {
        this.id = id;
        this.regionID = regionID;
        this.slotID = slotID;
        this.unitID = unitID;
        this.size = size;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionArmyEntity entity = (RegionArmyEntity) o;
        return id == entity.id && regionID == entity.regionID && slotID == entity.slotID && unitID == entity.unitID && size == entity.size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, regionID, slotID, unitID, size);
    }
}
