package database.enitites;

public final class RegionEntity {

    public final int id;
    public final int playerID;
    public final int income;
    public final int factoryLevel;
    public final int fortLevel;
    public final int towerLevel;


    public RegionEntity(int id, int playerID, int income, int factoryLevel, int fortLevel, int towerLevel) {
        this.id = id;
        this.playerID = playerID;
        this.income = income;
        this.factoryLevel = factoryLevel;
        this.fortLevel = fortLevel;
        this.towerLevel = towerLevel;
    }
}
