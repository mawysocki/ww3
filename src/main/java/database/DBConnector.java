package database;

import java.sql.*;

public class DBConnector {

    protected static Connection conn;
    protected static Statement statement;
    private static final String url = "jdbc:mysql://127.0.0.1:";
    private static final String port = "3306";
    private static final String database = "/ww3";
    private static final String user = "root";
    private static final String password = "";
    public static void startConnection() {
        //Ustawia połączenie jeżli go wcześniej nie było
        if (!isConnected()) {
            try {
                String dataBaseAddress = url + port + database;
                conn = DriverManager.getConnection(dataBaseAddress, user, password);
                if (isConnected()) {
                    System.out.println("Połączenie z bazą!");
                    statement = conn.createStatement();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

        }
    }

    public static boolean isConnected() {
        return conn != null;
    }
    //Wykonaie SELECT
    public static ResultSet executeQuery(String sql) {
        try {
            startConnection();
            return statement.executeQuery(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //Wykonanie INSERT/UPDATE/DELETE
    public static void update(String sql) {
        try {
            startConnection();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
