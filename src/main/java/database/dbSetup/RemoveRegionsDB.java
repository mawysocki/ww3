package database.dbSetup;

import database.DBConnector;

public class RemoveRegionsDB {

    //Skrypt usuwający wszystkie regiony.
    //Niezbędne przed tworzeniem nowych od początku
    public static void main(String[] args) {
        removeAllRegions();
    }
    public static void removeAllRegions() {
        String sqlRemoveAll = "DELETE FROM `regions`";
        DBConnector.update(sqlRemoveAll);
    }
}
