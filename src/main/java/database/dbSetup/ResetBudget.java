package database.dbSetup;

import database.DBConnector;

public class ResetBudget {

    //Reset budżetu dla wszystkich graczy
    public static void main(String[] args) {
        String sqlUpdate = "UPDATE players SET budget = 0";
        DBConnector.update(sqlUpdate);
    }
}
