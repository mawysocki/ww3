package database.dbSetup;

import database.DBConnector;

public class ResetRegionsDB {
    public static void main(String[] args) {
        resetAllRegions();
    }

    //Aktualizuje bazę danych robiąc dzieląc między dwóch graczę regiony po połowie
    public static void resetAllRegions() {
        String sqlUpdateCountry1 = "UPDATE regions SET player_id = 1 WHERE (id-1) % 10 < 5";
        String sqlUpdateCountry2 = "UPDATE regions SET player_id = 2 WHERE (id-1) % 10 >= 5";
        DBConnector.update(sqlUpdateCountry1);
        DBConnector.update(sqlUpdateCountry2);
    }
}
