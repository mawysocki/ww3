package database.dbSetup;

import database.DBConnector;

import static database.dbSetup.RemoveRegionsDB.removeAllRegions;

public class CreateRegionsDB {
    public static void main(String[] args) {
        //Przed stworzeniem nowych regionów należy usunąć wszystkie stare
        removeAllRegions();
        createAllRegions(50);
    }

    //Tworzenie wszystkich regionów z domyślnymi ustawieniami
    public static void createAllRegions(int numberOfRegions) {
        StringBuilder sqlCreateRegions = new StringBuilder("INSERT INTO `regions` (`id`, `player_id`, `income`, `factory`, `fort`, `tower`) VALUES");
        for (int id = 1; id <= numberOfRegions; id++) {
            //Ustala właściciela regionu po ID
            int countryID = (id-1) % 10 < 5 ? 1: 2;
            String value = String.format("('%s', '%s', '1000', '0', '0', '0'),", id, countryID);
            sqlCreateRegions.append(value);
        }
        //Pozbywa się niechcianego przecinka na końcu query
        sqlCreateRegions.append(";");
        String sqlClean = sqlCreateRegions.toString().replace(",;", "");
        DBConnector.update(sqlClean);
    }
}
