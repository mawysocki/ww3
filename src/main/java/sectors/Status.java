package sectors;

import utils.Texts;

public enum Status {

    LOW(Texts.TOO_LOW),
    MONEY(Texts.NO_MONEY),
    EXISTS(Texts.EXISTS),
    READY(Texts.BUY);

    final String text;

    Status(String s) {
        this.text = s;
    }
}
