package sectors;

import buildings.ArmyBuildings;
import buildings.IndustryBuildings;
import frames.RegionActionFrame;
import geo.Region;
import run.Settings;
import utils.Texts;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class MilitarySector implements Sector {
    JPanel firstRowPanel;
    JPanel secondRowPanel;
    JPanel thirdRowPanel;
    JComboBox<ArmyBuildings>[] slots;

    @Override
    public RegionActionFrame createSector(RegionActionFrame frame) {
        JPanel sectorPanel = new JPanel(new GridLayout(3, 1));
        sectorPanel.setPreferredSize(new Dimension(Settings.REGION_SIZE.width * 4, Settings.REGION_SIZE.height));

        firstRowPanel = new JPanel(new BorderLayout());
        secondRowPanel = new JPanel(new BorderLayout());
        thirdRowPanel = new JPanel(new BorderLayout());

        slots = new JComboBox[3];
        slots[0] = new JComboBox<>();
        JButton buildSlot1Button = new JButton(Texts.BUILD);
        JLabel availability1 = new JLabel(Status.EXISTS.text);
        firstRowPanel.add(slots[0], BorderLayout.WEST);
        firstRowPanel.add(availability1, BorderLayout.CENTER);
        firstRowPanel.add(buildSlot1Button, BorderLayout.EAST);

        slots[1] = new JComboBox<>();
        JButton buildSlot2Button = new JButton(Texts.BUILD);
        JLabel availability2 = new JLabel(Status.READY.text);
        secondRowPanel.add(slots[1], BorderLayout.WEST);
        secondRowPanel.add(availability2, BorderLayout.CENTER);
        secondRowPanel.add(buildSlot2Button, BorderLayout.EAST);

        slots[2] = new JComboBox<>();
        JButton buildSlot3Button = new JButton(Texts.BUILD);
        JLabel availability3 = new JLabel(Status.LOW.text);
        thirdRowPanel.add(slots[2], BorderLayout.WEST);
        thirdRowPanel.add(availability3, BorderLayout.CENTER);
        thirdRowPanel.add(buildSlot3Button, BorderLayout.EAST);

        slots[0].setFont(Sector.FONT);
        buildSlot1Button.setFont(Sector.FONT);
        slots[1].setFont(Sector.FONT);
        buildSlot2Button.setFont(Sector.FONT);
        slots[2].setFont(Sector.FONT);
        buildSlot3Button.setFont(Sector.FONT);

        sectorPanel.add(firstRowPanel);
        sectorPanel.add(secondRowPanel);
        sectorPanel.add(thirdRowPanel);
        frame.add(sectorPanel);
        return frame;
    }

    @Override
    public void verifySector(Region region) {
        verifyIndustryLevel(region);
    }

    //Sprawdzenie kryteriów widoczności slotów do stawiania budowli:
    //A - spełniony odpowiedni poziom przemysłu
    //B - Zapełnienie poprzednich slotów
    private void verifyIndustryLevel(Region region) {
        IndustryBuildings industry = region.buildingsInRegion.industry;
        switch (industry) {
            case INDUSTRY_L0:
                firstRowPanel.setVisible(false);
            case INDUSTRY_L1:
                secondRowPanel.setVisible(false);
            case INDUSTRY_L2: {
                thirdRowPanel.setVisible(false);}
            case INDUSTRY_L3: {
                //Dla poziomu 3 sloty do budowy poziomu 2 i 3 mogą być nadal niewidoczne
                //jeżli nie powstał jeszcze poziom 1
                checkBuildLine(secondRowPanel, region);
                checkBuildLine(thirdRowPanel, region);
            }
            default:
                break;
        }
        setAllBuyOptions(region);
    }

    private void setAllBuyOptions(Region region) {
        List<ArmyBuildings> list = region.getArmyBuildingsList();
        setBuyOptions(slots[0], null);
        for (int i = 1; i < list.size(); i++) {
            setBuyOptions(slots[i], list.get(i));
        }
    }

    //Ustawia listę z dostępnymi budynkami do wybudowania.
    private void setBuyOptions(JComboBox<ArmyBuildings> jComboBox, ArmyBuildings armyBuilding) {
        //Jeżeli nie stoi żaden budynek to są dostępne wszystkie podstawowe opcje
        if (armyBuilding == null) {
            jComboBox.addItem(ArmyBuildings.BARRACK_LV1);
            jComboBox.addItem(ArmyBuildings.TANKS_FACTORY_LV1);
        } else {
            //jeżli budynek istnieje to można wybudować jego zaawansowaną wersję
            jComboBox.addItem(armyBuilding.newBuild);
        }
    }

    //Blokuje możliwości wyświetlania 3 linii budynków jeżli nie zostaał wybudowana druga linia
    private void checkBuildLine(JPanel panel, Region region) {
        if (region.buildingsInRegion.industry.level - 2 >= region.getArmyBuildingsList().size()) {
            panel.setVisible(false);
        }
    }
}
