package sectors;

import buildings.BuildingsInRegion;
import frames.RegionActionFrame;
import game.Turn;
import geo.Region;
import upgrades.DefenceUpgradeListener;
import upgrades.IndustryUpgradeListener;
import upgrades.SpyUpgradeListener;
import utils.Texts;

import javax.swing.*;
import java.awt.*;

public class IndustrySector implements Sector{

    private JButton industryUpgradeButton;
    private JButton defenceUpgradeButton;

    private JButton spyUpgradeButton;

    //Tworzenie wszystkich kontrolek odpowiedzialnych za szczegły przemysłu w regionie
    @Override
    public RegionActionFrame createSector(RegionActionFrame frame) {
        JPanel sectorPanel = new JPanel(new GridLayout(3, 1));
        JPanel firstRowPanel = new JPanel(new BorderLayout());
        JPanel secondRowPanel = new JPanel(new BorderLayout());
        JPanel thirdRowPanel = new JPanel(new BorderLayout());

        JLabel industryLabel = new JLabel("Upgrade Industry");
        industryUpgradeButton = new JButton(Texts.UPGRADE);
        industryUpgradeButton.addActionListener(new IndustryUpgradeListener(frame.region, this));
        firstRowPanel.add(industryLabel, BorderLayout.WEST);
        firstRowPanel.add(industryUpgradeButton, BorderLayout.EAST);

        JLabel fortLabel = new JLabel(Texts.BUILD);
        defenceUpgradeButton = new JButton(Texts.UPGRADE);
        defenceUpgradeButton.addActionListener(new DefenceUpgradeListener(frame.region, this));
        secondRowPanel.add(fortLabel, BorderLayout.WEST);
        secondRowPanel.add(defenceUpgradeButton, BorderLayout.EAST);

        JLabel towerLabel = new JLabel(Texts.BUILD_TOWER);
        spyUpgradeButton = new JButton(Texts.BUILD);
        spyUpgradeButton.addActionListener(new SpyUpgradeListener(frame.region, this));
        thirdRowPanel.add(towerLabel, BorderLayout.WEST);
        thirdRowPanel.add(spyUpgradeButton, BorderLayout.EAST);

        industryLabel.setFont(Sector.FONT);
        industryUpgradeButton.setFont(Sector.FONT);
        fortLabel.setFont(Sector.FONT);
        defenceUpgradeButton.setFont(Sector.FONT);
        towerLabel.setFont(Sector.FONT);
        spyUpgradeButton.setFont(Sector.FONT);

        sectorPanel.add(firstRowPanel);
        sectorPanel.add(secondRowPanel);
        sectorPanel.add(thirdRowPanel);

        frame.add(sectorPanel);
        return frame;
    }

    @Override
    public void verifySector(Region region) {
        verifyIndustryUpgrade(region.buildingsInRegion);
        verifyFortificationUpgrade(region.buildingsInRegion);
        verifyTowerBuild(region.buildingsInRegion);
    }

    private void verifyIndustryUpgrade(BuildingsInRegion buildings) {
        if (buildings.industry.newBuild != null) {
            int price = buildings.industry.newBuild.price;
            boolean canBuild = Turn.currentPlayer.isEnoughMoney(price);
            industryUpgradeButton.setEnabled(canBuild);
        }
        else {
            industryUpgradeButton.setEnabled(false);
        }
    }

    private void verifyFortificationUpgrade(BuildingsInRegion buildings) {
        if (buildings.defence.newBuild != null) {
            int price = buildings.defence.newBuild.price;
            boolean canBuild = Turn.currentPlayer.isEnoughMoney(price);
            defenceUpgradeButton.setEnabled(canBuild);
        }
        else {
            defenceUpgradeButton.setEnabled(false);
        }
    }

    private void verifyTowerBuild(BuildingsInRegion buildings) {
        if (buildings.spy.newBuild != null) {
            int price = buildings.spy.newBuild.price;
            boolean canBuild = Turn.currentPlayer.isEnoughMoney(price);
            spyUpgradeButton.setEnabled(canBuild);
        }
        else {
            spyUpgradeButton.setEnabled(false);
        }
    }
}
