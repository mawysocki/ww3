package sectors;

import frames.RegionActionFrame;
import geo.Region;

import java.awt.*;

public interface Sector {

    RegionActionFrame createSector(RegionActionFrame frame);
    void verifySector(Region region);
    Font FONT = new Font("SansSerif", Font.PLAIN, 20);

}
