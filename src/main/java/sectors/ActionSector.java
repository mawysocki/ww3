package sectors;

import actionListeners.*;
import frames.RegionActionFrame;
import geo.Region;
import utils.Texts;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class ActionSector implements Sector {

    private ArmySlot[] slots;
    private final int slotSize = 4;
    JButton[] actionButtons;
    JButton confirmButton;

    @Override
    public RegionActionFrame createSector(RegionActionFrame frame) {
        JPanel sectorPanel = new JPanel(new BorderLayout());
        JPanel actionsPanel = new JPanel(new BorderLayout());
        JPanel slotPanel = new JPanel(new GridLayout(2, 2));
        buildButtons();
        actionsPanel.add(actionButtons[0], BorderLayout.WEST);
        actionsPanel.add(actionButtons[1], BorderLayout.CENTER);
        actionsPanel.add(actionButtons[2], BorderLayout.EAST);

        sectorPanel.add(actionsPanel, BorderLayout.NORTH);

        slots = new ArmySlot[slotSize];
        for (int i = 0; i < slotSize; i++) {
            slots[i] = new ArmySlot(i);
            slots[i].setFont(Sector.FONT);
            slotPanel.add(slots[i]);
        }

        new SelectUnitListener(actionButtons).setListeners(slots);
        actionButtons[0].addActionListener(new MoveListener(frame, slots, actionButtons));
        actionButtons[1].addActionListener(new DefenceListener(frame, slots, actionButtons));
        actionButtons[2].addActionListener(new SupportListener(frame, slots, actionButtons));
        sectorPanel.add(slotPanel, BorderLayout.CENTER);

        confirmButton = new JButton(Texts.CONFIRM);
        confirmButton.addActionListener(new ConfirmActionListener(frame));
        sectorPanel.add(confirmButton, BorderLayout.SOUTH);
        frame.add(sectorPanel);
        return frame;
    }

    private void buildButtons() {
        JButton moveButton = new JButton("Move");
        moveButton.setEnabled(false);
        JButton defenceButton = new JButton("Defence");
        defenceButton.setEnabled(false);
        JButton supportButton = new JButton("Support");
        supportButton.setEnabled(false);
        moveButton.setPreferredSize(new Dimension(100, 40));
        defenceButton.setPreferredSize(new Dimension(50, 40));
        supportButton.setPreferredSize(new Dimension(100, 40));
        actionButtons = new JButton[3];
        actionButtons[0] = moveButton;
        actionButtons[1] = defenceButton;
        actionButtons[2] = supportButton;
    }
    @Override
    public void verifySector(Region region) {
        for (int i = 0; i < region.units.length; i++) {
            slots[i].unit = region.units[i];
            boolean isUsed = slots[i].unit != null;
            if (isUsed) {
                String text = String.format("%s (%s)", slots[i].unit.getName(), slots[i].unit.getSize());
                slots[i].setText(text);
            }
            slots[i].setVisible(isUsed);

            blockPlanning(region.isPlanned());

        }
    }
    private void blockPlanning(boolean isPlanned) {
        if (isPlanned) {
            confirmButton.setEnabled(false);
            for (ArmySlot slot : slots) {
                slot.setEnabled(false);
            }
        }
    }
    public static ArrayList<ArmySlot> getSelectedUnits(ArmySlot[] slots) {
        System.out.println("Selected units: " );
        ArrayList<ArmySlot> newslots = new ArrayList<>();
        for (ArmySlot slot : slots) {
            if (slot.isSelected()) {
                newslots.add(slot);
            }
        }
        for (ArmySlot newslot : newslots) {
            System.out.println(newslot.unit.getName());
        }
        return newslots;
    }

    //Blokuje możliwość ponownego wybrania jednostki jeżli poddała się ona już planowaniu
    public static void disablePlannedUnits(ArrayList<ArmySlot> selectedSlots) {
        for (ArmySlot selectedSlot : selectedSlots) {
            selectedSlot.setEnabled(false);
            //Wybrany checkbox musi być ponownie odznaczony, aby nie został omyłkowo pobrany do ponownego planowania
            selectedSlot.setSelected(false);
        }
    }

    public static void disableActionButton(JButton[] actionButtons, boolean isEnabled) {
        Arrays.stream(actionButtons).forEach(jButton -> jButton.setEnabled(isEnabled));
    }
}
