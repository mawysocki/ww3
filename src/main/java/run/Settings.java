package run;

import java.awt.*;

public class Settings {

    //Uruchachamia grę z oknem opcji lub domyślnymi ustawieniami
    public static final boolean settingsEnabled = true;
    public static final boolean isServer = false;
    public static final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
    public static final Dimension REGION_SIZE = new Dimension(100, 100);
    public static final int COUNTRY_SIZE = 5;
    public static final int NUMBER_OF_PLAYERS = 2;
    public static final Dimension BATTLEFIELD_SIZE = new Dimension(REGION_SIZE.width * COUNTRY_SIZE * 2, REGION_SIZE.height * COUNTRY_SIZE);
}
