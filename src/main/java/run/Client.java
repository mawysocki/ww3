package run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

//PoC
public class Client {

    static int indexClient = -1;

    public static void main(String[] args) throws IOException {



        Socket socket = new Socket("localhost", 6700);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        // Wysyłanie wiadomości do serwera
        out.println("Hi, server!");

        // Odbieranie odpowiedzi od serwera
        String response;
//        = in.readLine();
//        indexClient = Integer.parseInt(response);
        out.write("Ok I am player " + indexClient);

        while ((response = in.readLine()) != null) {
            System.out.println("Otrzymano wiadomosc od servera: " + response);
        }
        socket.close();
    }
}
