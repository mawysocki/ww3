package run;

import java.awt.*;
import java.util.HashMap;

public class PlayerSettings {

    //Przechowuje informacje o ustawieniach początkowych graczy tj kolor czy nazwa
    //Robi to za pomocą map z ID gracza jako kluczami
    private static HashMap<Integer, Color> playerColors;
    private static HashMap<Integer, String> playerCountryNames;

    public final static Color defaultColor1 = Color.ORANGE;
    public final static Color defaultColor2 = Color.cyan;
    public final static String defaultCountryName1 = "GERMANY";
    public static String defaultCountryName2 = "POLAND";


    public static void setPlayerColor(Integer playerID, Color color) {
        if (playerColors == null) {
            playerColors = new HashMap<>();
        }
        playerColors.put(playerID, color);
    }

    public static Color getPlayerColor(Integer playerID) {
        if (playerColors == null) {
            playerColors = new HashMap<>();
            playerColors.put(1, Color.ORANGE);
            playerColors.put(2, Color.cyan);
        }
        return playerColors.get(playerID);
    }

    public static void setCountryName(Integer playerID, String name) {
        if (playerCountryNames == null) {
            playerCountryNames = new HashMap<>();
        }
        playerCountryNames.put(playerID, name);
    }

    public static String getCountryName(Integer playerID) {
        if (playerCountryNames == null) {
            playerCountryNames = new HashMap<>();
            playerCountryNames.put(1, defaultCountryName1);
            playerCountryNames.put(2, defaultCountryName2);
        }
        return playerCountryNames.get(playerID);
    }
}
