package run;

import frames.GameFrame;
import frames.GameOptions;


import static run.Settings.isServer;
import static run.Settings.settingsEnabled;

public class Runner {

    public static void main(String[] args) {
        //Jeśli config jest właczony to uruchamia się dodatkowe okno z opcjami gry
        if (settingsEnabled) {
            new GameOptions();
        }
        else {
            new GameFrame(isServer).start();
        }
    }
}
