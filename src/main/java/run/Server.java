package run;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

//PoC
public class Server {


    static BufferedReader[] in;
    static PrintWriter[] out;
    static ServerSocket serverSocket;
//    private Socket clientSocket;

//    static PrintWriter writer;
    static int numberOfPlayers = 0;
//    BufferedReader in1, in2;
//    PrintWriter out1, out2;
    static Socket[] clients;

    public static void start() {
        clients = new Socket[2];
        in = new BufferedReader[2];
        out = new PrintWriter[2];
        try {
            serverSocket = new ServerSocket(6700);

            while (numberOfPlayers < 2) {
                clients[numberOfPlayers] = serverSocket.accept();
                in[numberOfPlayers] = new BufferedReader(new InputStreamReader(clients[numberOfPlayers].getInputStream()));
                out[numberOfPlayers] = new PrintWriter(clients[numberOfPlayers].getOutputStream(), true);
                numberOfPlayers++;
//                clients[numberOfPlayers - 1] = new ClientHandler(clientSocket);
//                clients[numberOfPlayers - 1].start();

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public static void sendMessage(int index, String message) {
        out[index].println(message);
    }

//    public static void sendMessage(int index, String message) {
//        clients[index].sendMessage(message);
//    }
//
//    public static void readMessage(int index) {
//        clients[index].readMessage();
//    }
}

class ClientHandler extends Thread {
    private Socket clientSocket;
    BufferedReader in;
    PrintWriter out;

    public ClientHandler(Socket socket) {
        this.clientSocket = socket;
    }

    public void run() {
        try {
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new PrintWriter(clientSocket.getOutputStream(), true);


            out.write(Server.numberOfPlayers);


//            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        out.write(message);
    }

    public void readMessage() {
        String message;
        try {
            while ((message = in.readLine()) != null) {
                System.out.println(message);
            }
        } catch (IOException e) {

        }

    }
}
