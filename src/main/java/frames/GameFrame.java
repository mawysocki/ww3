package frames;

import game.Turn;
import panels.BattleMapPanel;
import panels.GameInfoPanel;
import run.Settings;

import javax.swing.*;
import java.awt.*;

public class GameFrame extends JFrame {

    BattleMapPanel battleMapPanel;
    private GameInfoPanel gameInfoPanel;

    public GameFrame() {
        this(false);
    }

    public GameFrame(boolean isServer) {
        //Server PoC
//        if (isServer) {
//            new WaitForOpponentFrame().waitFor();
//        }
        initFrame();
        initFramePanels();

        add(battleMapPanel, BorderLayout.WEST);
        add(gameInfoPanel, BorderLayout.CENTER);
        Controllers.setInstance(this, battleMapPanel, gameInfoPanel);
        Turn.startGame();

    }

    public void start() {
        setVisible(true);
        JOptionPane.showMessageDialog(battleMapPanel, "Select Capitol");
    }

    private void initFrame() {
        setTitle("WW3");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setMinimumSize(Settings.SCREEN_SIZE);
        setPreferredSize(Settings.SCREEN_SIZE);
        setResizable(false);
    }

    private void initFramePanels() {
        battleMapPanel = new BattleMapPanel();
        battleMapPanel.createRegions();
        gameInfoPanel = new GameInfoPanel();
    }
}
