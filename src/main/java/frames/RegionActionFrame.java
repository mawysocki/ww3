package frames;

import actions.ArmyAction;
import geo.Region;
import sectors.ActionSector;
import sectors.IndustrySector;
import sectors.MilitarySector;
import sectors.Sector;

public class RegionActionFrame extends AbstractRegionActionFrame {

    protected RegionActionFrame(Region region) {
        super(region);
    }
    //Tymczasowy bufor do przechowywania informacji o wykonywanej akcji w danym regionie
    public static ArmyAction[] armyActionsTemp;
    public static void createActionFrame(Region region) {
        AbstractRegionActionFrame.createActionFrame(region);
        Sector sector;
        sector = new IndustrySector();
        frame = sector.createSector(frame);
        sector.verifySector(frame.region);
        sector = new MilitarySector();
        frame = sector.createSector(frame);
        sector.verifySector(frame.region);
        sector = new ActionSector();
        frame = sector.createSector(frame);
        sector.verifySector(frame.region);
        initArmyActions();

    }
    //Przy utworzeniu się okna akcje dla regionu zostają zresetowane
    // jeżli nie było potwierdzenia o zakończonym planowaniu dla regionu
    private static void initArmyActions() {
        if (!frame.region.isPlanned()) {
            armyActionsTemp = new ArmyAction[4];
        }
    }

    //Ustawia akcję jednostek w kolejności w jakiej one występują w regionie
    public static void setArmyActionValue(int index, ArmyAction armyAction) {
        armyActionsTemp[index] = armyAction;
    }
}
