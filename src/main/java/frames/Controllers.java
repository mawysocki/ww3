package frames;

import geo.Country;
import geo.Region;
import panels.BattleMapPanel;
import panels.GameInfoPanel;
import panels.RegionInfoPanel;
import panels.TurnInfoPanel;

import java.awt.*;

//Klasa odpowiedzialna za aktualizowanie danych w panelach informacyjnych i oknach
//Dzięki singletonowi i metodom statycznym dostęp do kontrolek jest z każdego miejsca
//Klasa przekazuje dane bezpońrednio do kontrolek, które są prywatne
public class Controllers {

    private static Controllers controllers;

    private final GameFrame gameFrame;
    private final BattleMapPanel battleMapPanel;
    private final TurnInfoPanel turnInfoPanel;
    private final RegionInfoPanel regionInfoPanel;

    private Controllers(GameFrame gameFrame, GameInfoPanel gameInfoPanel, BattleMapPanel battleMapPanel) {
        this.gameFrame = gameFrame;
        this.turnInfoPanel = gameInfoPanel.turnInfoPanel;
        this.regionInfoPanel = gameInfoPanel.regionInfoPanel;
        this.battleMapPanel = battleMapPanel;
    }

    public static void setInstance(final GameFrame gameFrame, final BattleMapPanel battleMapPanel, final GameInfoPanel gameInfoPanel) {
        if (controllers == null) {
            controllers = new Controllers(gameFrame, gameInfoPanel, battleMapPanel);
        }
    }

    public static void updateRegionInfo(Region region) {
        controllers.regionInfoPanel.updateLabels(region);
    }

    //Zarządza informacjami o graczu
    public static void updateTurnInfo(Country country) {
        controllers.turnInfoPanel.updateTurnLabel(country);
        controllers.turnInfoPanel.updateRemainRegionLabel(controllers.battleMapPanel.countRegions(country));
        controllers.turnInfoPanel.updateBudgetLabel();
    }

    //Zarządza przyciskami do tury i rekrutacji
    public static void updateTurnInfo(boolean isEnabled) {
        controllers.turnInfoPanel.setButtonsEnabled(isEnabled);
    }

    public static void setBoardEnabled(boolean isEnabled) {
        controllers.gameFrame.setEnabled(isEnabled);
    }

    public static void resetPlanningFlag() {controllers.battleMapPanel.resetPlanningFlag();}

    public static Region getRegion(Point address) {
        return controllers.battleMapPanel.getRegion(address);
    }

}
