package frames;

import run.Server;

import javax.swing.*;

public class WaitForOpponentFrame extends JFrame {

    public WaitForOpponentFrame() {
        super("Waiting for the opponent");

    }

    public void waitFor() {
        this.setVisible(true);
        this.setSize(500, 1);
//        Server.start();
        this.setVisible(false);
        this.dispose();
    }
}
