package frames;
import actionListeners.gameSettings.ApplyOptionsListener;
import actionListeners.gameSettings.ConfirmPlayerNamesListener;
import actionListeners.gameSettings.DefaultColorOptionListener;
import actionListeners.gameSettings.SelectPlayerColorListener;
import run.PlayerSettings;
import run.Settings;

import javax.swing.*;
import java.awt.*;

import static run.Settings.isServer;

public class GameOptions extends JFrame {

    //Okno opcji dzieli się na 3 sekcje: header, body, footer
    private JPanel header;
    private JPanel body;

    //W sekcji body znajdują się opcje wyboru
    private JPanel colorPanel;
    private JPanel namesPanel;

    //Footer to panel przechowujący przycisk zatwierdzania opcji
    private JPanel footer;

    //Składowe panelu wyboru koloru
    private JCheckBox defaultColors;
    private JButton player1Color;
    private JButton player2Color;

    //Składowe panelu wyboru nazw
    private TextField player1Name;
    private TextField player2Name;
    private JButton confirmNamesButton;
    private JButton applyButton;
    public GameOptions() {
        setTitle("Options");
        Dimension size = new Dimension(Settings.SCREEN_SIZE.width/2, Settings.SCREEN_SIZE.height/4);
        setUndecorated(true);
        setMinimumSize(size);
        setPreferredSize(size);
        setLocation(new Point(100,100));
        initPanels();
        setPanels();
        setVisible(true);
    }

    public void closeOptions() {
        setVisible(false);
        new GameFrame(isServer).start();
    }

    private void initButtons() {
        header = new JPanel();
        body = new JPanel();
        //Panele wchodzące w skład body mają być ułożone jedno pod drugim
        body.setLayout(new BoxLayout(body, BoxLayout.Y_AXIS));
        defaultColors = new JCheckBox("Set default color");
        defaultColors.setSelected(true);
        player1Color = new JButton("Player 1");
        player1Color.setEnabled(false);
        player2Color = new JButton("Player 2");
        player2Color.setEnabled(false);

        colorPanel = new JPanel();
        player1Name = new TextField(PlayerSettings.defaultCountryName1);
        player2Name = new TextField(PlayerSettings.defaultCountryName2);
        namesPanel = new JPanel();
        footer = new JPanel();
        confirmNamesButton = new JButton("Confirm names");
        applyButton = new JButton("Apply Settings");
    }

    private void initPanels() {
        initButtons();
        header.add(new JLabel("Select Player Settings"));
        body.add(colorPanel);
        body.add(namesPanel);

        colorPanel.add(new JLabel("Select Color"));

        //Wciśnięcie przycisku otwiera okno wyboru koloru
        player1Color.addActionListener(new SelectPlayerColorListener(player1Color, 1));
        player2Color.addActionListener(new SelectPlayerColorListener(player2Color, 2));
        colorPanel.add(player1Color);
        colorPanel.add(player2Color);
        defaultColors.addItemListener(new DefaultColorOptionListener(player1Color, player2Color));
        colorPanel.add(defaultColors);

        namesPanel.add(player1Name);
        namesPanel.add(player2Name);
        //Nasłuchuje potwierdzenia, że nazwy zostały wybrane
        confirmNamesButton.addActionListener(new ConfirmPlayerNamesListener(player1Name, player2Name, confirmNamesButton));
        namesPanel.add(confirmNamesButton);

        applyButton.addActionListener(new ApplyOptionsListener(this, player1Color, player2Color, confirmNamesButton));
        footer.add(applyButton);
    }


    private void setPanels() {
        //Dzieli header, body i footer w odpowiednich proporcjach, aby body było największe
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // Pierwszy panel
        gbc.weighty = 1;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(header, gbc);

        // Drugi panel - 3 razy większy
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(body, gbc);

        // Trzeci panel
        gbc.weighty = 0;
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(footer, gbc);
    }
}
