package frames;

import geo.Region;
import run.Settings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class AbstractRegionActionFrame extends JFrame {

    public final Region region;
    protected static RegionActionFrame frame;

    protected AbstractRegionActionFrame(Region region) {
        this.region = region;
        this.setTitle(region.getName());
    }

    public static void createActionFrame(Region region) {
        frame = new RegionActionFrame(region);
        frame.setBounds(getFrameBounds(region));
        frame.addWindowListener(new RegionActionFrameListener());
        frame.setVisible(true);
        frame.setLayout(new GridLayout(3,1));
    }

    protected static Rectangle getFrameBounds(Region clickedRegion) {
        Point p = clickedRegion.getLocation();
        int width = Settings.REGION_SIZE.width*5;
        int heigth = Settings.REGION_SIZE.height*5;
        return new Rectangle(p.x, p.y, width, heigth);
    }

    static class RegionActionFrameListener extends WindowAdapter {
        @Override
        public void windowOpened(WindowEvent e) {
            Controllers.setBoardEnabled(false);
        }

        @Override
        public void windowClosing(WindowEvent e) {
            Controllers.setBoardEnabled(true);
        }
    }
}
