package frames;

import actions.Actions;
import actions.ArmyAction;
import geo.NewAddressOption;
import geo.Region;
import sectors.ActionSector;
import sectors.ArmySlot;
import sectors.Sector;
import run.Settings;
import utils.Texts;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.Objects;

//Okno wyboru ruchu zaznaczonych jednostek
public class UnitMoveFrame extends JFrame {
    private RegionActionFrame frame;
    private List<ArmySlot> selectedSlots;
    private JButton[] actionButtons;
    private NewAddressOption options;

    private JButton confirmButtom;
    //Okno działa tak samo dla akcji MOVE oraz SUPPORT. Różni się tylko tym parametrem akcji
    public UnitMoveFrame(RegionActionFrame frame, List<ArmySlot> armySlotList, JButton[] actionButtons, Actions action) {
        this.frame = frame;
        this.selectedSlots = armySlotList;
        this.actionButtons = actionButtons;
        this.setLayout(new BorderLayout());
        this.setTitle(Texts.DESTINATION);
        this.setBounds(getFrameBounds(frame.region));
        prepareOptionList(frame.region);
        addConfirmButton(action);
        this.addWindowListener(new UnitMoveFrameListener());
        this.setVisible(true);
    }

    protected Rectangle getFrameBounds(Region region) {
        Point p = region.getLocation();
        int width = Settings.REGION_SIZE.width*3;
        int heigth = Settings.REGION_SIZE.height;
        return new Rectangle(p.x, p.y, width, heigth);
    }

    private void prepareOptionList(Region region) {
        options = new NewAddressOption(region.getNeighbours());
        options.setFont(Sector.FONT);
        for (Region neighbour : options.regionOptions) {
            if (neighbour!=null) {
                options.addItem(neighbour.getName());
            }
        }
        this.add(options, BorderLayout.NORTH);

    }

    public void closeWindow() {
        this.dispose();
        frame.setEnabled(true);
    }

    private void addConfirmButton(Actions action) {
        confirmButtom = new JButton(Texts.CONFIRM);
        confirmButtom.addActionListener(new UnitMoveConfirmListener(action));
        this.add(confirmButtom, BorderLayout.SOUTH);
    }
    class UnitMoveConfirmListener implements ActionListener {

        private final Actions action;
        //Przekazuje akcje w zależności czy został wywołany z przycisku MOVE czy SUPPORT
        public UnitMoveConfirmListener(Actions action) {
            this.action = action;
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            for (ArmySlot armySlot : selectedSlots) {
                RegionActionFrame.setArmyActionValue(armySlot.id, createArmyAction(armySlot));
                armySlot.setEnabled(false);
                armySlot.setSelected(false);
            }
            closeWindow();
            displayConfirmedActionsInRegion();
            ActionSector.disableActionButton(actionButtons, false);
        }
        private ArmyAction createArmyAction(ArmySlot armySlot) {
            Point oldAddress = frame.region.getRegionAddress();
            Point newAddress = options.findByName(Objects.requireNonNull(options.getSelectedItem()).toString()).getRegionAddress();
            return new ArmyAction(armySlot, action, oldAddress,newAddress);
        }
        private void displayConfirmedActionsInRegion() {
            System.out.println("Confirmed moves:");
            for (ArmyAction armyAction : RegionActionFrame.armyActionsTemp) {
                if (armyAction != null) {
                    String name = armyAction.slot.unit.getName();
                    int oldX = armyAction.oldAddress.x;
                    int oldY = armyAction.oldAddress.y;
                    String text;
                    if (!armyAction.action.equals(Actions.DEFENCE)) {
                        int newX = armyAction.newAddress.x;
                        int newY = armyAction.newAddress.y;
                        text = String.format("%s %s from x=%s y=%s to x=%s y=%s", name, armyAction.action, oldX, oldY, newX, newY);
                    }
                    else {
                        text = String.format("%s %s on x=%s y=%s", name, armyAction.action, oldX, oldY);
                    }
                    System.out.println(text);
                }
                else {
                    System.out.println("Empty..");
                }
            }
        }
    }

    class UnitMoveFrameListener extends WindowAdapter {
        @Override
        public void windowOpened(WindowEvent e)  {
            frame.setEnabled(false);
        }

        @Override
        public void windowClosing(WindowEvent e) {
            frame.setEnabled(true);
        }
    }
}
