package game;

import actions.*;

import java.util.ArrayList;
import java.util.List;

//Klasa odpowiedzialna za przechowywania i wykonywanie ruchów obydwu graczy
public class Logistic {

    private List<ArmyAction>[] moveActions;
    private List<ArmyAction>[] defenceActions;
    private List<ArmyAction>[] supportActions;


    public Logistic() {
        this.moveActions = new List[Turn.players.length];
        this.defenceActions = new List[Turn.players.length];
        this.supportActions = new List[Turn.players.length];
    }

    public void separateActions() {
        System.out.println("Seperate Actions");
        for (int i = 0; i < Turn.players.length; i++) {
            moveActions[i] = new ArrayList<>();
            defenceActions[i] = new ArrayList<>();
            supportActions[i] = new ArrayList<>();
            for (RegionAction regionAction : Planning.getPlan(Turn.players[i]).regionActions) {
                for (ArmyAction action : regionAction.actions) {
                    if (action != null) {
                        System.out.println(String.format("%s: %s", Turn.players[i].country, action));
                        switch (action.action) {
                            case MOVE ->
                                moveActions[i].add(action);
                            case DEFENCE ->
                                defenceActions[i].add(action);
                            case SUPPORT ->
                                supportActions[i].add(action);
                        }
                    }
                }
            }
        }
        displaySeparatedActions();
    }

    public void displaySeparatedActions() {
        System.out.println("Display separated actions");
        System.out.println("Move..");
        displayOrderedMoveAction();
//        displayActions(moveActions);
//        System.out.println("Defence..");
//        displayActions(defenceActions);
//        System.out.println("Support..");
//        displayActions(supportActions);
    }

    private void displayActions(List<ArmyAction>[] actions) {
        for (int i = 0; i < Turn.players.length; i++) {
            System.out.println("Player: " + Turn.players[i].country);
            for (ArmyAction moveAction : actions[i]) {
                System.out.println(moveAction);
            }
        }
    }

    private void displayOrderedMoveAction() {
        int length = findTheLongestQueue();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < Turn.players.length; j++) {
                if (moveActions[j].size() <= i) {
                    continue;
                }
                System.out.println(moveActions[j].get(i));
            }
        }
    }

    private int findTheLongestQueue() {
        int length = 0;
        for (int i = 0; i < Turn.players.length; i++) {
            int temp = moveActions[i].size();
            length = temp > length ? temp : length;
        }
        return length;
    }


    public void moveInOrder() {
        int length = findTheLongestQueue();
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < Turn.players.length; j++) {
                if (moveActions[j].size() <= i) {
                    continue;
                }
                //Wymaga jeszcze sprawdzenia czy dana jednostka nie została zniszczona przez jakąś inną akcję przeciwnika
                ArmyAction currentAction = moveActions[j].get(i);

                new MoveManager(currentAction, defenceActions, supportActions).move();
            }
        }
    }
}
