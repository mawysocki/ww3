package game;

import database.scripts.PlayerDB;
import geo.Country;

public class Player {
    public final int id;
    public final Country country;
    private boolean isCapitolSelected;

    private int budget;

    public Player(int id) {
        this.id = id;
        this.country = Country.getCountry(id);
        this.budget = PlayerDB.getPlayerBudget(this.id);
    }


    public boolean isCapitolSelected() {
        return isCapitolSelected;
    }

    public void setCapitolSelected(boolean capitolSelected) {
        isCapitolSelected = capitolSelected;
    }

    public int getBudget() {
        return budget;
    }
    public void addIncome(int income) {
        budget += income;
    }
    public void pay(int price) {
        budget -= price;
    }

    public boolean isEnoughMoney(int price) {
        return budget >= price;
    }
}
