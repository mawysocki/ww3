package game;

import actions.Planning;
import database.scripts.PlayerDB;
import frames.Controllers;
import run.Server;

import static run.Settings.NUMBER_OF_PLAYERS;
import static run.Settings.isServer;

public class Turn {

    public static Player currentPlayer;
    public static Player[] players;
    static int currentTurn = 1;
    static boolean isStarted = false;

    public static void startGame() {
        if (!isStarted) {
            createPlayers(NUMBER_OF_PLAYERS);
            Controllers.updateTurnInfo(currentPlayer.country);
            Planning.startPlanning();
            isStarted = true;
        }
    }

    private static void createPlayers(int numberOfPlayers) {
        players = new Player[numberOfPlayers];
        for (int id = 1; id <= numberOfPlayers; id++) {
            players[id-1] = new Player(id);
        }
        currentPlayer = checkTurn();
    }

    private static Player checkTurn() {
        int index = (currentTurn - 1) % players.length;
        if (isServer) {
            Server.sendMessage(index, "Current turn: " + players[index].country);
        }
        return players[index];
    }

    public static boolean isLastPlayer() {
        return currentTurn % players.length == 0;
    }

    public static void startNextTurn() {
        currentPlayer.addIncome(PlayerDB.getDailyIncome(currentPlayer.id));
        PlayerDB.updatePlayerBudget(currentPlayer.id, currentPlayer.getBudget());
        currentTurn++;
        currentPlayer = checkTurn();
        Controllers.updateTurnInfo(currentPlayer.country);


    }
}
