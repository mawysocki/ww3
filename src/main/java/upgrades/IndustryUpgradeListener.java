package upgrades;

import geo.Region;
import sectors.IndustrySector;

import java.awt.event.ActionEvent;

public class IndustryUpgradeListener extends AbstractUpgradeListener {

    public IndustryUpgradeListener(Region region, IndustrySector sector) {
        super(region, sector);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        buildIndustry();
    }
}
