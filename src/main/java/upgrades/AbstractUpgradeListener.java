package upgrades;

import geo.Region;
import sectors.IndustrySector;
import sectors.Sector;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class AbstractUpgradeListener implements ActionListener {
    protected final Region region;
    protected final Sector sector;

    public AbstractUpgradeListener(Region region, IndustrySector sector) {
        this.region = region;
        this.sector = sector;
    }

    protected void buildIndustry() {
        region.buildingsInRegion.build(region.buildingsInRegion.industry);
        sector.verifySector(region);
    }

    protected void buildDefence() {
        region.buildingsInRegion.build(region.buildingsInRegion.defence);
        sector.verifySector(region);
    }
    protected void buildSpy() {
        region.buildingsInRegion.build(region.buildingsInRegion.spy);
        sector.verifySector(region);
    }
}
