package upgrades;

import geo.Region;
import sectors.IndustrySector;

import java.awt.event.ActionEvent;

public class DefenceUpgradeListener extends AbstractUpgradeListener{
    public DefenceUpgradeListener(Region region, IndustrySector sector) {
        super(region, sector);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        buildDefence();
    }
}
