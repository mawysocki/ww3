package upgrades;

import geo.Region;
import sectors.IndustrySector;

import java.awt.event.ActionEvent;

public class SpyUpgradeListener extends AbstractUpgradeListener{
    public SpyUpgradeListener(Region region, IndustrySector sector) {
        super(region, sector);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        buildSpy();
    }
}
