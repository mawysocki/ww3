package geo;

import run.PlayerSettings;

import java.awt.*;

public enum Country {
    //Kraje nie są autozdefiniowane
    //Posiadają pola do flagi i nazwę
    //Mają domyślne wartości wzięte z konfiguracji
    COUNTRY1(1, PlayerSettings.getCountryName(1),  PlayerSettings.getPlayerColor(1)),
    COUNTRY2(2, PlayerSettings.getCountryName(2), PlayerSettings.getPlayerColor(2));

    public final int id;
    public final String name;
    public final Color flag;
    Country(int id, String name, Color flag) {
        this.id = id;
        this.name = name;
        this.flag = flag;
    }

    public static Country getCountry(int id) {
        return switch (id) {
            case 1 -> COUNTRY1;
            case 2 -> COUNTRY2;
            default -> null;
        };
    }
}
