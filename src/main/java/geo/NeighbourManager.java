package geo;

import run.Settings;

public class NeighbourManager {

    public static Region[][] setNeighbours(Region[][] regions) {
        for (int i = 0; i < Settings.COUNTRY_SIZE; i++) {
            for (int j = 0; j < Settings.COUNTRY_SIZE * 2; j++) {
                setNorthNeighbour(regions, i, j);
                setSouthNeighbour(regions, i, j);
                setWestNeighbour(regions, i, j);
                setEastNeighbour(regions, i, j);
            }
        }
        return regions;
    }

    private static void setNorthNeighbour(Region[][] regions, int i, int j) {
        if (i > 0) {
            regions[i][j].north = regions[i-1][j];
        }

    }

    private static void setSouthNeighbour(Region[][] regions, int i, int j) {
        if (i < Settings.COUNTRY_SIZE-1) {
            regions[i][j].south = regions[i+1][j];
        }
    }

    private static void setWestNeighbour(Region[][] regions, int i, int j) {
        if (j > 0) {
            regions[i][j].west = regions[i][j-1];
        }
    }

    private static void setEastNeighbour(Region[][] regions, int i, int j) {
        if (j < Settings.COUNTRY_SIZE*2-1) {
            regions[i][j].east = regions[i][j+1];
        }
    }
}
