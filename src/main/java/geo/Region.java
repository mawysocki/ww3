package geo;

import buildings.ArmyBuildings;
import buildings.BuildingsInRegion;
import database.enitites.RegionEntity;
import database.scripts.RegionDB;
import database.scripts.UnitDB;
import game.Turn;
import units.*;
import utils.Texts;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Region extends AbstractRegion {

    private Country country;
    private boolean isCapitol = false;
    private int dailyIncome;
    private int defenceBonus = 0;
    private boolean isPlanned = false;

    private List<ArmyBuildings> armyBuildingsList;
    public BuildingsInRegion buildingsInRegion;

    public Unit[] units = new Unit[4];


    public Region(Point point) {
        super(point);
        this.addMouseListener(new RegionActionListener());
        buildingsInRegion = new BuildingsInRegion();
        armyBuildingsList = new ArrayList<>();
        RegionEntity regionEntity = RegionDB.getRegionEntity(regionNumber);
        if (regionEntity != null) {
            dailyIncome = regionEntity.income;
            setCountry(Country.getCountry(regionEntity.playerID));
        }
        else {
            dailyIncome = 1000;
            setCountry(Country.COUNTRY2);
        }



//        System.out.println(regionNumber);

//        if (regionNumber == 5) {
//            units = UnitDB.setUnitsInRegion(regionNumber);
//        }
        units = UnitDB.setUnitsInRegion(regionNumber);
        //Tylko do testów
//        units[0] = new Soldier();
//        units[1] = new Soldier();
//        units[3] = new BWP();
    }
    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
        this.setBackground(this.country.flag);
    }

    public int getDailyIncome() {
        return dailyIncome;
    }

    public void setCapitol(boolean isCapitol) {
        this.isCapitol = isCapitol;
    }

    public boolean isCapitol() {
        return isCapitol;
    }

    public void updateIncome() {
        RegionEntity regionEntity = RegionDB.getRegionEntity(regionNumber);
        if (regionEntity != null) {dailyIncome = regionEntity.income;}

        if (isCapitol) {
            dailyIncome *= 2;
        }
    }

    public List<ArmyBuildings> getArmyBuildingsList() {
        return armyBuildingsList;
    }

    public int getFactoryNumbers() {
        return armyBuildingsList.size();
    }

    public int getDefenceBonus() {
        switch (buildingsInRegion.defence) {
            case FORT_L1:
                defenceBonus = 3;
                break;
            case FORT_L2:
                defenceBonus = 6;
                break;
            case FORT_L3:
                defenceBonus = 10;
                break;
            default:
                defenceBonus = 0;
                break;
        }
        if (isCapitol) {
            defenceBonus += 5;
        }
        return defenceBonus;
    }

    public boolean isEmptyRegion() {
        for (Unit unit : units) {
            if (unit != null) {
                return false;
            }
        }
        return true;
    }
    public boolean isOwnerShip() {
        return Turn.currentPlayer.country.equals(this.country);
    }

    public boolean isPlanned() {
        return isPlanned;
    }

    public void setPlanned(boolean planned) {
        isPlanned = planned;
    }

    //Dodaje nową zaprzyjaźnioną jednostkę do istniejącego garnizonu
    public boolean addNewArmy(Unit unit) {
        //Sprawdzenie czy istnieje już jednostka tego samego typu aby je połaczyć
        int sameTypeIndex = findTheSameType(unit);
        //Sprawdzenie czy istnieje wolna przestrzeń w garnizonie
        int emptyPlaceIndex = findEmptyPlace();
        //Jako priorytet trakruje się połączenie jednostek z istniejącymi
        if (sameTypeIndex >= 0) {
            units[sameTypeIndex].mergeUnits(unit);
            return true;
            //Jeśli nie ma takich samych jednostek to szuka się wolnego pola
        } else if (emptyPlaceIndex >= 0) {
            units[emptyPlaceIndex] = unit;
            return true;
        } else {
            //Jesli nie ma również wolnego pola to jednostka musi wrócić do siebie
            String messsage = Texts.NO_SPACE + this.name;
            JOptionPane.showMessageDialog(null, messsage);
            return false;
        }
    }

    private int findTheSameType(Unit unit) {
        for (int i = 0; i < units.length; i++) {
            if (units[i] != null && units[i].isTheSameType(unit)) {
                return i;
            }
        }
        return -1;
    }

    private int findEmptyPlace() {
        for (int i = 0; i < units.length; i++) {
            if (units[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public void removeUnit(Unit unit) {
        for (int i = 0; i < units.length; i++) {
            if (unit.equals(units[i])) {
                units[i] = null;
                break;
            }
        }
    }

    //Sprawdza czy dwa regiony są sobie wrogie
    public boolean isFriendlyRegion(Region region) {
        return this.getCountry().equals(region.getCountry());
    }
}
