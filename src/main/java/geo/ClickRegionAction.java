package geo;

public interface ClickRegionAction {

    void click(Region region);
}
