package geo;

import run.Settings;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public abstract class AbstractRegion extends JButton {
    protected final String name;
    protected Point address;

    protected final int regionNumber;
    public Region north;
    public Region south;
    public Region west;
    public Region east;
    public AbstractRegion(Point address) {
        this.address = address;
        this.regionNumber = countRegionNumber(address);
        String title = String.format("Region: %s", regionNumber);
        this.name = title;
        this.setText(title);
    }

    public int getRegionNumber() {
        return regionNumber;
    }
    @Override
    public String getName() {
        return name;
    }

    public Region[] getNeighbours() {
        Region[] regions = new Region[4];
        regions[0] = north;
        regions[1] = south;
        regions[2] = west;
        regions[3] = east;
        return regions;
    }

    public Point getRegionAddress() {
        return address;
    }

    protected int countRegionNumber(Point address) {
        return  address.x * Settings.COUNTRY_SIZE * 2 + address.y + 1;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractRegion)) return false;
        AbstractRegion that = (AbstractRegion) o;
        return name.equals(that.name) && address.equals(that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, address);
    }
}
