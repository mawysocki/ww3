package geo;

import javax.swing.*;

public class NewAddressOption extends JComboBox {

    public Region[] regionOptions;

    public NewAddressOption(Region[] regionOptions) {
        this.regionOptions = regionOptions;
    }

    public Region findByName(String name) {
        for (Region regionOption : regionOptions) {
            if (regionOption != null) {
                if (regionOption.getName().equals(name)) {
                    return regionOption;
                }
            }
        }
        return null;
    }
}
