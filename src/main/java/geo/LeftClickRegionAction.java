package geo;

import frames.Controllers;
import game.Turn;
import utils.Texts;

import javax.swing.*;

public class LeftClickRegionAction implements ClickRegionAction{
    public void leftClick(Region clickedRegion) {

    }

    public void setCapitol(Region clickedRegion) {
        clickedRegion.setCapitol(true);
        Turn.currentPlayer.setCapitolSelected(true);
        clickedRegion.updateIncome();
        //Nazwę krajue bierze z pola a nie bezpośrednio z Enuma
        System.out.printf("Capitol in %s for %s%n", clickedRegion.getName(), Turn.currentPlayer.country.name);

    }

    public void selectRegion(Region clickedRegion) {
//        clickedRegion.setCountry(Turn.currentPlayer.country);
        Controllers.updateRegionInfo(clickedRegion);
//        display(clickedRegion.north);
//        display(clickedRegion.south);
//        display(clickedRegion.west);
//        display(clickedRegion.east);

//        Turn.startNextTurn();
    }

    private void display(Region region) {
        if (region != null) {
            System.out.println(region.getName());
        }
    }

    @Override
    public void click(Region clickedRegion) {
        if (!Turn.currentPlayer.isCapitolSelected()) {
            if (clickedRegion.isOwnerShip()) {
                setCapitol(clickedRegion);
                if (Turn.isLastPlayer()) {
                    Controllers.updateTurnInfo(true);
                }
                Turn.startNextTurn();
            } else {
                JOptionPane.showMessageDialog(null, Texts.INCORRECT_COUNTRY_FOR_CAPITOL);
            }

        } else {
            selectRegion(clickedRegion);
        }
    }
}
