package geo;

import java.awt.event.*;

//Klasa przechytująca informacje o kliknięciu danego regionu
//W zależnosci od rodzaju kliknięcia wybiera podjętą akcję
public class RegionActionListener extends MouseAdapter {
    @Override
    public void mousePressed(MouseEvent e) {
        Object source = e.getSource();
        Region clickedRegion = (Region) source;
        ClickRegionAction action = getAction(e);
        action.click(clickedRegion);
    }

    private ClickRegionAction getAction(MouseEvent mouseEvent) {
        switch (mouseEvent.getButton()) {
            case MouseEvent.BUTTON1 -> {
                return new LeftClickRegionAction();
            }
            case MouseEvent.BUTTON3 -> {
                return new RightClickRegionAction();
            }
            default ->
                    throw new IllegalStateException("Action not allowed");
        }
    }
}
