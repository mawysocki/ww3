package geo;

import frames.RegionActionFrame;
import game.Turn;

public class RightClickRegionAction implements ClickRegionAction {
    @Override
    public void click(Region clickedRegion) {
        if (Turn.currentPlayer.isCapitolSelected() && clickedRegion.isOwnerShip()) {
            RegionActionFrame.createActionFrame(clickedRegion);
        }
    }
}
