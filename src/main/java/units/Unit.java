package units;

import database.scripts.UnitDB;
import database.enitites.UnitEntity;

public class Unit {
    protected String name;

    public final Units type;
    protected int size;
    protected int defaultSize;
    protected  int defaultAttack;
    protected  int defaultDefence;
    protected  int defaultCounterAttack;



    protected int attack;
    protected int defence;
    protected int counterAttack;
    protected int hp;
    protected int damage;

    public Unit(Units type) {
        UnitEntity entity = UnitDB.getUnitEntity(type.id);
        this.type = type;
        this.name = type.name();

        this.hp = entity.hp;
        this.size = entity.size;
        this.defaultAttack = entity.attack;
        this.defaultDefence = entity.defence;
        this.damage = entity.damage;

//        System.out.println(this.name);
//        System.out.println(this.size);
//        System.out.println(this.hp);
//        System.out.println(this.damage);
    }
    public Unit(Units type, int size) {
        this(type);
        this.size = size;
    }




    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public int getAttack() {
        return attack;
    }

    public int getDefence() {
        return defence;
    }

    public int getCounterAttack() {
        return counterAttack;
    }

    public int getDamage() {
        return damage;
    }
    public int getUnitHP() {
        return size * hp;
    }

    public void setAttack(int bonus) {
        attack = defaultAttack + bonus;
    }
    public void setDefence(int bonus) {
        defence = defaultDefence + bonus;
    }
    public void setCounterAttack(int bonus) {
        counterAttack = defaultCounterAttack + bonus;
    }

    public boolean isTheSameType(Unit unit) {
        return this.type.equals(unit.type);
    }
    public void mergeUnits(Unit unit) {
        if (!isTheSameType(unit)) {
            throw new IllegalStateException("Cannot merge different units");
        }
        this.size += unit.size;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Unit object)) {
            return false;
        }
        boolean isTheSameName = this.name.equals(object.name);
        boolean isTheSameSize = this.size == object.size;
        return isTheSameName && isTheSameSize;
    }

    @Override
    public String toString() {
        return "Unit{" +
                "type='" + type + '\'' +
                ", size=" + size +
                ", hp=" + hp +
                '}';
    }
}
