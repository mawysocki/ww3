package units;

public enum Units {

    SOLDIER(1),
    MOTORIZED_SOLDIER(2),
    SPECIAL_FORCE(3),
    BWP(4),
    TANK(5),
    ABRAMS(6);
    public final int id;

    Units(int id) {
        this.id = id;
    }
}
