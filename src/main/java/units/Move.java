package units;

public interface Move {
    Unit collapse(Unit newUnit);
    Unit split(Unit newUnit);
}
