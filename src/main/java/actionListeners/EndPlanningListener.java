package actionListeners;

import actions.ArmyAction;
import actions.Planning;
import actions.RegionAction;
import database.scripts.ArmyDB;
import frames.Controllers;
import game.Logistic;
import game.Player;
import game.Turn;
import utils.Texts;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EndPlanningListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Turn.isLastPlayer()) {
            JOptionPane.showMessageDialog(null, Texts.END_TURN);
            endTurn();
            Controllers.resetPlanningFlag();
            Planning.startPlanning();
        }

        Turn.startNextTurn();
    }

    private void endTurn() {
        Logistic logistic = new Logistic();
        logistic.separateActions();
        logistic.moveInOrder();
        //Aktualizacja pozycji wojsk w bazie danych
        ArmyDB.updateArmy();
//        displayAllActions();
    }

    private void displayAllActions() {
        System.out.println("Display all actions");
        for (Player player : Turn.players) {
            System.out.println("Player: " + player.country);
            for (RegionAction regionAction : Planning.getPlan(player).regionActions) {
                for (ArmyAction action : regionAction.actions) {
                    if (action != null) {
                        System.out.println(action);
                    }
                }
            }
        }
    }
}
