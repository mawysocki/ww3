package actionListeners;

import actions.ArmyAction;
import actions.Planning;
import frames.Controllers;
import frames.RegionActionFrame;
import game.Turn;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfirmActionListener implements ActionListener {
    private RegionActionFrame frame;
    public ConfirmActionListener(RegionActionFrame frame) {
        this.frame = frame;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        frame.region.setPlanned(true);
        //Przed zamknięciem należy dodać ArmyAction z buforu do starego planu
//        displayRegionActions();
        Planning.addRegionToPlan(Turn.currentPlayer);
        frame.dispose();
        Controllers.setBoardEnabled(true);
    }

    //Do testów
    private void displayRegionActions() {
        System.out.println(frame.region.getName());
        for (ArmyAction armyAction : RegionActionFrame.armyActionsTemp) {
            if (armyAction != null)
                System.out.println(armyAction);
        }
    }
}
