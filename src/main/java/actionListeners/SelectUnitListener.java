package actionListeners;

import sectors.ActionSector;
import sectors.ArmySlot;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//Sprawdza które jednostki zostały zaznaczone
public class SelectUnitListener implements ActionListener {

    private ArmySlot[] slots;
    JButton[] actionButtons;

    public SelectUnitListener(JButton[] actionButtons) {
        this.actionButtons = actionButtons;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setButtons();
    }

    public void setListeners(ArmySlot[] slots) {
        this.slots = slots;
        for (JCheckBox slot : this.slots) {
            slot.addActionListener(this);
        }
    }

    private void setButtons() {
        boolean isSelected = false;
        for (JCheckBox slot : slots) {
            if (slot.isSelected()) {
                isSelected = true;
                break;
            }
        }
        boolean finalIsSelected = isSelected;
        ActionSector.disableActionButton(actionButtons, finalIsSelected);
    }
}
