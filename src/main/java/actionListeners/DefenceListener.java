package actionListeners;

import actions.Actions;
import actions.ArmyAction;
import frames.RegionActionFrame;
import sectors.ActionSector;
import sectors.ArmySlot;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class DefenceListener extends AbstractActionListener {

    public DefenceListener(RegionActionFrame frame, ArmySlot[] slots, JButton[] actionButtons) {
        super(frame, slots, actionButtons);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<ArmySlot> selectedSlots = ActionSector.getSelectedUnits(slots);
        for (ArmySlot selectedSlot : selectedSlots) {
            ArmyAction action = new ArmyAction(selectedSlot, Actions.DEFENCE, frame.region.getRegionAddress(), null);
            RegionActionFrame.setArmyActionValue(selectedSlot.id, action);
        }
        ActionSector.disablePlannedUnits(selectedSlots);
        ActionSector.disableActionButton(actionButtons, false);
    }

}
