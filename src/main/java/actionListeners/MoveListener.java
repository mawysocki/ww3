package actionListeners;

import actions.Actions;
import frames.RegionActionFrame;
import frames.UnitMoveFrame;
import sectors.ActionSector;
import sectors.ArmySlot;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.List;

public class MoveListener extends AbstractActionListener{

    public MoveListener(RegionActionFrame frame, ArmySlot[] slots, JButton[] actionButtons) {
        super(frame, slots, actionButtons);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<ArmySlot> selectedSlots = ActionSector.getSelectedUnits(slots);
        new UnitMoveFrame(frame, selectedSlots, actionButtons, Actions.MOVE);
    }
}
