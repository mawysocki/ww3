package actionListeners.gameSettings;

import frames.GameOptions;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ApplyOptionsListener implements ActionListener {
    private final GameOptions frame;
    private final JButton player1;
    private final JButton player2;
    private final JButton confirmNames;

    public ApplyOptionsListener(GameOptions frame, JButton player1, JButton player2, JButton confirmNames) {
        this.frame = frame;
        this.player1 = player1;
        this.player2 = player2;
        this.confirmNames = confirmNames;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (player1.isEnabled() || player2.isEnabled() || confirmNames.isEnabled()) {
            JOptionPane.showMessageDialog(null, "Set color and country names");
        }
        else {
            frame.closeOptions();
        }
    }
}
