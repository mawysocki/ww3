package actionListeners.gameSettings;

import run.PlayerSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfirmPlayerNamesListener implements ActionListener {
    private  TextField player1;
    private  TextField player2;
    private JButton confirmNames;

    public ConfirmPlayerNamesListener(TextField player1, TextField player2, JButton confirmNames) {
        this.player1 = player1;
        this.player2 = player2;
        this.confirmNames = confirmNames;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        //Nasłuchiwanie potwierdzenia wyboru nazw
        //Jeśli nazwa jest pusta to nie akceptuje wyboru
        if (player1.getText().isEmpty() || player2.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Country name cannot be empty");
        }
        else {
            //Zatwierdzając wybór przypisuje go do mapy z nazwami gdzie kluczem jest ID gracza
            //Blokuje dalszą możliwość wyboru
            PlayerSettings.setCountryName(1, player1.getText().toUpperCase());
            PlayerSettings.setCountryName(2, player2.getText().toUpperCase());
            confirmNames.setEnabled(false);
            player1.setEnabled(false);
            player2.setEnabled(false);
        }
    }
}
