package actionListeners.gameSettings;

import run.PlayerSettings;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SelectPlayerColorListener implements ActionListener {
    private final Integer playerNumber;
    private JButton playerButton;

    public SelectPlayerColorListener(JButton playerButton, Integer playerNumber) {
        this.playerNumber = playerNumber;
        this.playerButton = playerButton;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        //Nasłuchuje wybór koloru i zapisuje go do mapy z ustawieniami
        Color color = JColorChooser.showDialog(null, "Wybierz kolor", Color.WHITE);
        PlayerSettings.setPlayerColor(playerNumber, color);
        this.playerButton.setEnabled(false);

    }
}
