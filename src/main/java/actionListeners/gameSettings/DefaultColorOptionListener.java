package actionListeners.gameSettings;

import run.PlayerSettings;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class DefaultColorOptionListener implements ItemListener {
    private JButton player1;
    private JButton player2;

    public DefaultColorOptionListener(JButton player1, JButton player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        //Zaznaczenie checkboxa powoduje blokadę możliwości wybrania koloru i brany jest domyślny
        boolean isSelected = e.getStateChange() == ItemEvent.SELECTED;
        player1.setEnabled(!isSelected);
        player2.setEnabled(!isSelected);
        if (isSelected) {
            //Ustawiając domyślny wybór zapisuje domyślne kolory
            PlayerSettings.setPlayerColor(1, PlayerSettings.defaultColor1);
            PlayerSettings.setPlayerColor(2, PlayerSettings.defaultColor2);
        }
    }
}
