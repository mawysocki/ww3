package actionListeners;

import frames.RegionActionFrame;
import sectors.ArmySlot;

import javax.swing.*;
import java.awt.event.ActionListener;

public abstract class AbstractActionListener implements ActionListener {
    protected RegionActionFrame frame;
    protected ArmySlot[] slots;

    protected JButton[] actionButtons;
    public AbstractActionListener(RegionActionFrame frame, ArmySlot[] slots, JButton[] actionButtons) {
        this.frame = frame;
        this.slots = slots;
        this.actionButtons = actionButtons;
    }

}
