package utils;

public interface Texts {
    String INCORRECT_COUNTRY_FOR_CAPITOL = "You cannot set your capitol on oppenent's land";
    String END_TURN = "The turn has ended";
    //Panel tury użytkownika
    String CURRENT_TURN = "Current turn: ";
    String REGIONS = "Regions: ";
    String BUDGET = "Budget: ";
    String RECRUITMENT = "Recruitment";
    String ENDPLANNING = "End planning";
    //Panel szczegółw regionu
    String REGION = "Current region: ";
    String NATIONALITY = "Nationality: ";
    String INCOME = "Income: ";
    String CAPITOL = "Capitol: ";
    String FACTORY = "Factory: ";
    String FORT = "Fort level: ";
    String DEFENCE = "Defence: ";
    String TOWER = "Tower: ";
    //Statusy rozwoju przemysłu
    String TOO_LOW = "Industry is too low";
    String NO_MONEY = "Not enough money";
    String EXISTS = "Object already exists";
    String BUY = "You can buy it";

    //Sectors
    String BUILD = "Build";
    String BUILD_TOWER = "Build tower";
    String CONFIRM = "Confirm";
    String UPGRADE = "+";
    String UPGRADE_FORT = "Upgrade Fortification";
    String EXCEPTION_LEVEL_MESSAGE = "Building does not have higher level";
    //Inne
    String DESTINATION = "Set destination";

    String NO_SPACE = "Cannot move to region.";
}
