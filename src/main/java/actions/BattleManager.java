package actions;

import frames.Controllers;
import geo.Region;
import units.Unit;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BattleManager {
    private List<ArmyAction>[] defenceActions;
    private List<ArmyAction>[] supportActions;
    private List<ArmyAction> supportAttackInvolved;
    private List<ArmyAction> supportDefenceInvolved;
    private List<ArmyAction> defenceInvolved;
    private List<Unit> attackArmy;
    private List<Unit> defenceArmy;

    public BattleManager(List<ArmyAction>[] defenceActions, List<ArmyAction>[] supportActions) {
        this.defenceActions = defenceActions;
        this.supportActions = supportActions;
    }

    public void fight(ArmyAction action) {
        collectActions(action);
        collectUnits(action);
        calculatePower();
    }


    private void collectActions(ArmyAction action) {
        supportAttackInvolved = getAttackSupport(action);
        supportDefenceInvolved = getDefenceSupport(action);
        defenceInvolved = getDefence(action);
    }

    private void collectUnits(ArmyAction action) {
        attackArmy = new ArrayList<>();
        attackArmy.add(action.slot.unit);
        System.out.println("Attacked Army: ");
        supportAttackInvolved.stream().map(e -> e.slot.unit).forEach(attackArmy::add);
        attackArmy.stream().map(Unit::toString).forEach(System.out::println);
        defenceArmy = new ArrayList<>();
        System.out.println("Defence Army: ");
        supportDefenceInvolved.stream().map(e -> e.slot.unit).forEach(defenceArmy::add);
        defenceInvolved.stream().map(e -> e.slot.unit).forEach(defenceArmy::add);
        defenceArmy.stream().map(Unit::toString).forEach(System.out::println);
    }

    private void calculatePower() {
        int attack = DamageCalculator.getAttack(attackArmy);
        int defence = DamageCalculator.getDefence(defenceArmy);
        int damage = DamageCalculator.getDamage(attackArmy, attack, defence);
    }

    private List<ArmyAction> getDefence(ArmyAction action) {
        List<ArmyAction> iteratedArray = new ArrayList<>();
        Region regionInvolved = Controllers.getRegion(action.newAddress);
        int index = getPlayerIndex(regionInvolved, defenceActions);
        if (index != -1) {
            for (ArmyAction armyAction : defenceActions[index]) {
                Region defenceActionRegion = Controllers.getRegion(armyAction.oldAddress);
                if (defenceActionRegion.equals(regionInvolved)) {
                    iteratedArray.add(armyAction);
                }
            }
        }
        return iteratedArray;
    }


    private List<ArmyAction> getAttackSupport(ArmyAction action) {
        return getSupport(action, action.oldAddress);
    }

    private List<ArmyAction> getDefenceSupport(ArmyAction action) {
        return getSupport(action, action.newAddress);
    }

    private List<ArmyAction> getSupport(ArmyAction action, Point address) {
        List<ArmyAction> iteratedArray = new ArrayList<>();
        //Do porównania potrzebny jest adress regionu atakującego/broniącego się
        Region regionInvolved = Controllers.getRegion(address);
        //Indeks gracza do którego należy atakujący/broniacy się region, aby nie szukać wsród wszystkich graczy
        int index = getPlayerIndex(regionInvolved, supportActions);
        //Sprawdzenie czy gracz w ogóle miał jakiś support
        if (index >= 0) {
            //Przejęcie po akcjach tylko pasującego gracza
            for (ArmyAction armyAction : supportActions[index]) {
                //Jesli address wsparcia pasuje do adresu atakowanego to ta akcja wspiera atak
                if (armyAction.newAddress.equals(action.newAddress)) {
                    iteratedArray.add(armyAction);
                }
            }
        }
        return iteratedArray;
    }

    private int getPlayerIndex(Region involvedRegion, List<ArmyAction>[] actions) {
        for (int i = 0; i < actions.length; i++) {
            for (ArmyAction action : actions[i]) {
                Region region = Controllers.getRegion(action.oldAddress);
                if (region.isFriendlyRegion(involvedRegion)) {
                    return i;
                }
                break;
            }
        }
        return -1;
    }
}
