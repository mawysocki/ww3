package actions;

import game.Player;
import game.Turn;

import java.util.HashMap;

public class Planning {

    private static HashMap<Player, PlayerPLan> plans;

    public static void startPlanning() {
        plans = new HashMap<>();
        for (Player player: Turn.players) {
            plans.put(player, new PlayerPLan());
        }
    }

    public static PlayerPLan getPlan(Player player) {
        return plans.get(player);
    }
    public static void addRegionToPlan(Player player) {
        getPlan(player).addPlannedRegion();
    }

}
