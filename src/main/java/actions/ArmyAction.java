package actions;

import sectors.ArmySlot;

import java.awt.*;

public final class ArmyAction {
    public final ArmySlot slot;
    public final Actions action;
    public final Point oldAddress;
    public final Point newAddress;

    public ArmyAction(ArmySlot slot, Actions action, Point oldAddress, Point newAddress) {
        this.slot = slot;
        this.action = action;
        this.oldAddress = oldAddress;
        this.newAddress = newAddress;
    }

    @Override
    public String toString() {
        int oldX = oldAddress.x;
        int oldY = oldAddress.y;
        String text = "";
        switch (action) {
            case MOVE, SUPPORT -> {
                int newX = newAddress.x;
                int newY = newAddress.y;
                text = String.format("%s %s from x=%s y=%s to x=%s y=%s", slot.unit.getName(), action, oldX, oldY, newX, newY);
            }
            case DEFENCE -> 
                text = String.format("%s %s in x=%s y=%s", slot.unit.getName(), action, oldX, oldY);
        }
        return text;
    }
}
