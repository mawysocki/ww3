package actions;

import units.Unit;

import java.util.List;

public class DamageCalculator {

    public static int getDamage(List<Unit> attackUnits, int attack, int defence) {
        double additionalDamage = attack > defence ? 0.05 : 0.025;
        double fullDamage = 0;
        for (Unit attackUnit : attackUnits) {
            fullDamage += attackUnit.getSize() * (1+(attack-defence)*additionalDamage) * attackUnit.getDamage();
        }
        System.out.println("Damage: " + fullDamage);
        return (int)fullDamage;
    }

    public static int getAttack(List<Unit> attackUnits) {
        double totalSize = 0;
        double totalAttack = 0;
        for (Unit unit : attackUnits) {
            //Brak bonusów póki co
            unit.setAttack(0);
            totalAttack +=  unit.getSize() * unit.getAttack();
            totalSize += unit.getSize();
        }
        System.out.println("Attack: " + totalAttack);
        System.out.println("Size: " + totalSize);
        System.out.println("Average attack: " + totalAttack/totalSize);
        return (int)(totalAttack/totalSize);
    }

    public static int getDefence(List<Unit> defenceUnits) {
        if (defenceUnits.isEmpty()) {
            return 0;
        }
        int totalSize = 0;
        int totalDefence = 0;
        for (Unit unit : defenceUnits) {
            totalDefence +=  unit.getSize() * unit.getDefence();
            totalSize += unit.getSize();
        }
        System.out.println("Average defence: " + totalDefence/totalSize);
        return totalDefence/totalSize;
    }
    public static int countRemainUnits(int units, int hp, int damage) {
        int loss = damage / hp;
        return units - loss;
    }
}
