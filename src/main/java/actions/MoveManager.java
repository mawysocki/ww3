package actions;

import database.scripts.RegionDB;
import frames.Controllers;
import geo.Region;

import java.util.List;

public class MoveManager {

    private ArmyAction action;
    private BattleManager battleManager;


    public MoveManager(ArmyAction action, List<ArmyAction>[] defenceActions, List<ArmyAction>[] supportActions) {
        this.action = action;
        this.battleManager = new BattleManager(defenceActions, supportActions);
    }

    public void move() {
        //Na bazie aktualnej akcji pobiera region źródłowu oraz docelowy
        Region oldRegion = Controllers.getRegion(action.oldAddress);
        Region newRegion = Controllers.getRegion(action.newAddress);
        System.out.println(String.format("%s from %s to %s", action.action, oldRegion.getName(), newRegion.getName()));
        System.out.println("Is our region: " + oldRegion.isFriendlyRegion(newRegion));
        //Sprawdza czy to zaprzyjaźniony region. Jeżli tak to próbuje tam dodać nową jednostkę
        if (oldRegion.isFriendlyRegion(newRegion)) {
            //Dodanie jednostki zwraca true/false, aby wiedzieć czy akcja sie udała
            boolean isAdded = newRegion.addNewArmy(action.slot.unit);
            //Tylko jeżli udało się dodać nową jednostkę, to jest ona usuwana ze starego pola
            if (isAdded) {
                oldRegion.removeUnit(action.slot.unit);
            }
        }
        else {
            //Wrogi region
            if (newRegion.isEmptyRegion()) {
                //Jednostka wchodzi na puste pole, więc nie ma sensu sprawdzać ponwonie czy jest wolne pole
                newRegion.addNewArmy(action.slot.unit);
                oldRegion.removeUnit(action.slot.unit);
                newRegion.setCountry(oldRegion.getCountry());
                //Aktualizacja stanów w bazie danych
                RegionDB.updateCountryForRegion(newRegion.getRegionNumber(), newRegion.getCountry().id);
            }
            else {
                battleManager.fight(action);
            }
        }
    }




}
