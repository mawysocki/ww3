package panels;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractInfoPanel extends JPanel {
    public AbstractInfoPanel() {
        this.setLayout(new GridLayout(10,1));
        this.setFont(new Font("SansSerif", Font.PLAIN, 30));
        createHeader();
        createLabels();
    }
    protected abstract void createHeader();
    protected abstract void createLabels();


}
