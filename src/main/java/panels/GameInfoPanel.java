package panels;

import javax.swing.*;
import java.awt.*;

//Klasa będąca panelem służy jedynie do przechowywania dwóch mniejszych paneli informacyjnych
//oraz układa je równo względem siebie oraz względem planszy
public class GameInfoPanel extends JPanel {

    public final TurnInfoPanel turnInfoPanel;
    public final RegionInfoPanel regionInfoPanel;
    public GameInfoPanel() {
        this.setLayout(new GridLayout(2,1));
        turnInfoPanel = new TurnInfoPanel();
        regionInfoPanel = new RegionInfoPanel();
        this.add(turnInfoPanel);
        this.add(regionInfoPanel);
    }
}
