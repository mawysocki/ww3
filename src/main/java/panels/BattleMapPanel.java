package panels;

import geo.Country;
import geo.NeighbourManager;
import geo.Region;
import run.Settings;

import javax.swing.*;
import java.awt.*;

public class BattleMapPanel extends JPanel {

    Region[][] regions;

    public BattleMapPanel() {
        this.setPreferredSize(Settings.BATTLEFIELD_SIZE);
        this.setLayout(new GridLayout(Settings.COUNTRY_SIZE, Settings.COUNTRY_SIZE * 2));
        regions = new Region[Settings.COUNTRY_SIZE][Settings.COUNTRY_SIZE * 2];
    }

    public void createRegions() {
        generateRegions();
        regions = NeighbourManager.setNeighbours(regions);
    }
    private void generateRegions() {
        for (int i = 0; i < Settings.COUNTRY_SIZE; i++) {
            for (int j = 0; j < Settings.COUNTRY_SIZE * 2; j++) {
                regions[i][j] = new Region(new Point(i, j));
                this.add(regions[i][j]);
            }
        }
    }

    public int countRegions(Country country) {
        int counter = 0;
        for (Region[] r1 : regions) {
            for (Region region : r1) {
                if (region.getCountry().equals(country)) {
                    counter++;
                }
            }
        }
        return counter;
    }

    public Region getRegion(Point address) {
        return regions[address.x][address.y];
    }
    public void resetPlanningFlag() {
        for (Region[] r1 : regions) {
            for (Region region : r1) {
                region.setPlanned(false);
            }
        }
    }
}
