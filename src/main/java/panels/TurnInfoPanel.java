package panels;

import actionListeners.EndPlanningListener;
import game.Turn;
import geo.Country;
import utils.Texts;

import javax.swing.*;

//Panel wyświetlający informacje o aktualnym użytkowniku
//Z tego miejsca można dokonać mobilizacji #todo
//Z tego miejsca użytkownik może zakończyć turę
public class TurnInfoPanel extends AbstractInfoPanel {
    private JLabel currentTurnLabel;
    private JLabel regionCounterLabel;
    private JLabel budgetLabel;
    private JButton recruitButton;
    private JButton endPlanningButton;

    public TurnInfoPanel() {
        super();
    }

    @Override
    protected void createHeader() {
        JButton gameInfoHeader = new JButton("Game info");
        gameInfoHeader.setFont(this.getFont());
        gameInfoHeader.setEnabled(false);
        this.add(gameInfoHeader);
    }
    @Override
    protected void createLabels() {
        currentTurnLabel = new JLabel(Texts.CURRENT_TURN);
        currentTurnLabel.setFont(this.getFont());
        regionCounterLabel = new JLabel(Texts.REGIONS);
        regionCounterLabel.setFont(this.getFont());
        budgetLabel = new JLabel(Texts.BUDGET);
        budgetLabel.setFont(this.getFont());
        recruitButton = new JButton(Texts.RECRUITMENT);
        recruitButton.setFont(this.getFont());
        endPlanningButton = new JButton(Texts.ENDPLANNING);
        endPlanningButton.setFont(this.getFont());

        //Na starcie przyciski mają być niedostępne póki nie zostanie wybrana stolica
        setButtonsEnabled(false);
        endPlanningButton.addActionListener(new EndPlanningListener());
        this.add(currentTurnLabel);
        this.add(regionCounterLabel);
        this.add(budgetLabel);
        //Dodatkowe przestrzenie, aby przyciski lepiej wyglądały
        this.add(new JPanel());
        this.add(new JPanel());
        this.add(recruitButton);
        this.add(new JPanel());
        this.add(new JPanel());
        this.add(endPlanningButton);
    }

    public void updateTurnLabel(Country country) {
        String text = String.format("%s %s", Texts.CURRENT_TURN, country.name);
        currentTurnLabel.setText(text);
    }
    public void updateRemainRegionLabel(int counter) {
        String text = String.format("%s %s", Texts.REGIONS, counter);
        regionCounterLabel.setText(text);
    }
    public void updateBudgetLabel() {
        String text = String.format("%s %s", Texts.BUDGET, Turn.currentPlayer.getBudget());
        budgetLabel.setText(text);
    }

    public void setButtonsEnabled(boolean isEnabled) {
        recruitButton.setEnabled(isEnabled);
        endPlanningButton.setEnabled(isEnabled);
    }
}
