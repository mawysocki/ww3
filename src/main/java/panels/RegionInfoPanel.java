package panels;

import geo.Region;
import utils.Texts;

import javax.swing.*;


//Panel wyświetlający informacje o konkretnym regionie
public class RegionInfoPanel extends AbstractInfoPanel {
    private JLabel currentRegion;
    private JLabel nationality;
    private JLabel incomeFromRegion;
    private JLabel capitol;
    private JLabel factories;
    private JLabel fort;
    private JLabel defence;
    private JLabel tower;

    public RegionInfoPanel() {
        super();
    }

    @Override
    protected void createHeader() {
        JButton regionInfoHeader = new JButton("Region info");
        regionInfoHeader.setFont(this.getFont());
        regionInfoHeader.setEnabled(false);
        this.add(regionInfoHeader);
    }

    @Override
    protected void createLabels() {
        this.currentRegion = createLabel(Texts.REGION);
        this.nationality = createLabel(Texts.NATIONALITY);
        this.incomeFromRegion = createLabel(Texts.INCOME);
        this.capitol = createLabel(Texts.CAPITOL);
        this.factories = createLabel(Texts.FACTORY);
        this.fort = createLabel(Texts.FORT);
        this.defence = createLabel(Texts.DEFENCE);
        this.tower = createLabel(Texts.TOWER);
    }

    private JLabel createLabel(String title) {
        JLabel label = new JLabel(title);
        label.setFont(this.getFont());
        this.add(label);
        return label;
    }

    public void updateLabels(Region region) {
        this.currentRegion.setText(String.format("%s %s", Texts.REGION, region.getName()));
        this.nationality.setText(String.format("%s %s", Texts.NATIONALITY, region.getCountry().name));
        this.incomeFromRegion.setText(String.format("%s %s", Texts.INCOME, region.getDailyIncome()));
        this.capitol.setText(String.format("%s %s", Texts.CAPITOL, region.isCapitol()));
        this.factories.setText(String.format("%s %s/%s", Texts.FACTORY, region.getFactoryNumbers(), region.buildingsInRegion.industry.level));
        this.fort.setText(String.format("%s %s", Texts.FORT, region.buildingsInRegion.defence.level));
        this.defence.setText(String.format("%s %s", Texts.DEFENCE, region.getDefenceBonus()));
        this.tower.setText(String.format("%s %s", Texts.TOWER, region.buildingsInRegion.spy));
    }
}
