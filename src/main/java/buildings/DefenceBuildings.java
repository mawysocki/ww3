package buildings;

public enum DefenceBuildings {

    FORT_L3(50000, null, 3),
    FORT_L2(25000, FORT_L3, 2),
    FORT_L1(10000, FORT_L2, 1),
    FORT_L0(0, FORT_L1, 0);

    public final int price;
    public final DefenceBuildings newBuild;
    public final int level;

    DefenceBuildings(int price, DefenceBuildings newBuild, int level) {
        this.price = price;
        this.newBuild = newBuild;
        this.level = level;
    }
}
