package buildings;

public enum ArmyBuildings {
    BARRACK_LV3(35000, 2, null),
    BARRACK_LV2(25000,2, BARRACK_LV3),
    BARRACK_LV1(15000,1, BARRACK_LV2),
    TANKS_FACTORY_LV3(55000, 7, null),
    TANKS_FACTORY_LV2(40000, 4, TANKS_FACTORY_LV3),
    TANKS_FACTORY_LV1(25000, 2, TANKS_FACTORY_LV2);

//    AIRPORT_LV3(10);
    //    AIRPORT_LV2(6),
//    AIRPORT_LV1(3),
    public final int price;
    public final int productionTime;
    public final ArmyBuildings newBuild;
    ArmyBuildings(int price, int productionTime, ArmyBuildings newBuild) {
        this.price = price;
        this.productionTime = productionTime;
        this.newBuild = newBuild;
    }
}
