package buildings;

public enum SpyBuildings {
    TOWER(30000, null),
    EMPTY(0, TOWER);

    public final int price;
    public final SpyBuildings newBuild;

    SpyBuildings(int price, SpyBuildings newBuild) {
        this.price = price;
        this.newBuild = newBuild;
    }
}
