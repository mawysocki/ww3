package buildings;

public enum IndustryBuildings {
    INDUSTRY_L3(30000, null, 3),
    INDUSTRY_L2(20000, INDUSTRY_L3, 2),
    INDUSTRY_L1(10000, INDUSTRY_L2, 1),
    INDUSTRY_L0(0, INDUSTRY_L1, 0);

    public final int price;
    public final IndustryBuildings newBuild;
    public final int level;

    IndustryBuildings(int price, IndustryBuildings newBuild, int level) {
        this.price = price;
        this.newBuild = newBuild;
        this.level = level;
    }
}
