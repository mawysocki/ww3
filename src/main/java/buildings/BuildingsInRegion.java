package buildings;

import utils.Texts;

public class BuildingsInRegion {

    public IndustryBuildings industry;
    public DefenceBuildings defence;
    public SpyBuildings spy;

    //Ustawienie wartości początkowych dla budynków
    public BuildingsInRegion() {
        industry = IndustryBuildings.INDUSTRY_L0;
        defence = DefenceBuildings.FORT_L0;
        spy = SpyBuildings.EMPTY;
    }

    //Przeciążona metoda w zależności jakiego typu budynek jest ulepszany
    public void build(IndustryBuildings upgraded) {
        if (upgraded.newBuild != null) {
            industry = upgraded.newBuild;
        } else {
            throw new IllegalStateException(Texts.EXCEPTION_LEVEL_MESSAGE);
        }
    }
    public void build(DefenceBuildings upgraded) {
        if (upgraded.newBuild != null) {
            defence = upgraded.newBuild;
        } else {
            throw new IllegalStateException(Texts.EXCEPTION_LEVEL_MESSAGE);
        }
    }
    public void build(SpyBuildings upgraded) {
        if (upgraded.newBuild != null) {
            spy = upgraded.newBuild;
        } else {
            throw new IllegalStateException(Texts.EXCEPTION_LEVEL_MESSAGE);
        }
    }
}
